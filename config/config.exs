use Mix.Config
config :logger, level: :info

config :cluster_kv,
  topologies: [
    kv: [
      strategy: Cluster.Strategy.Epmd,
      config: [
        hosts: [
          :"app@test1.com",
          :"app@test2.com",
          :"app@test3.com"
        ]
      ],
      connect: {:net_kernel, :connect_node, []},
      disconnect: {:erlang, :disconnect_node, []},
      list_nodes: {:erlang, :nodes, [:connected]}
    ]
  ]
