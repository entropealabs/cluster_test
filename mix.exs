defmodule ClusterTest.MixProject do
  use Mix.Project

  def project do
    [
      app: :cluster_test,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      releases: releases(),
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {ClusterTest, []}
    ]
  end

  defp releases do
    [
      app: [
        include_executables_for: [:unix],
        applications: [cluster_test: :permanent, runtime_tools: :permanent],
        cookie: "cluster_test_cookie"
      ]
    ]
  end

  defp deps do
    [
      # {:wampex, path: "../wampex"}
      {:wampex,
       git: "https://gitlab.com/entropealabs/wampex.git",
       tag: "a1eb395a239310fb97e8f6eaf08e143846693637"}
    ]
  end
end
