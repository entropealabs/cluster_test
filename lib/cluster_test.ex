defmodule ClusterTest do
  @moduledoc """
  Documentation for ClusterTest.
  """
  use Application

  def start(_type, _opts) do
    topologies = Application.get_env(:cluster_kv, :topologies)

    children = [
      {Wampex.Router,
       [name: ClusterRouter, port: 4000, topologies: topologies, replicas: 3, quorum: 2]}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: ClusterTest.Supervisor)
  end
end
